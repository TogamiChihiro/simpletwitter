package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	private static final String INVALID_PARAMETER = "不正なパラメータが入力されました";

	//つぶやきの編集(表示)
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String messageIdStr = request.getParameter("messageId");
		int messageId;
		Message message = null;

		if ((!StringUtils.isEmpty(messageIdStr) && (messageIdStr.matches("^[0-9]+$")))){
			messageId = Integer.parseInt(messageIdStr);
			message = new MessageService().select(messageId);
		}

		if (message == null) {
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add(INVALID_PARAMETER);
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("top.jsp").forward(request, response);
			return;
		}
		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	//つぶやきの編集(処理)
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();

		String text = request.getParameter("text");
		Message message = new Message();

		message.setText(text);

		if (!isValid(text, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}

		String id = request.getParameter("id");
		message.setId(Integer.parseInt(id));
		new MessageService().update(message);

		response.sendRedirect("./");
	}

	private boolean isValid(String text, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
